#!/usr/bin/python
# Python 2/3 compatibility
from __future__ import print_function         # IMPORTANDO A PARTE __future__ DA BIBLIOTECA print_function
import cv2                                    # IMPORTANDO A BIBLIOTECA cv2
import numpy as np                            # IMPORTANDO A BIBLIOTECA numpy E APELIDADNDO COMO np


def filter_mask(im_filter_mask):                                           # CRIANDO UMA FUNCAO filter_mask E CRIANDO A VARIAVEL PRIVADA im_filter_mask
    kernel = np.ones((5, 5), np.uint8)                                       # CRIANDO A VARIAVEL kernel, PEGANDO A FUNCAO ones DA BIBLIOTECA np E DANDO VALORES A ELA. SEU OBJETIVO E DEFINIR A INTENSIDADE DO FILTRO erode
    erode = cv2.erode(im_filter_mask, kernel, iterations=1)                 # CRIANDO A VARIAVEL erode, PEGANDO A FUNCAO erode DA BIBLIOTECA cv2, E IMPLEMENTANDO ESSA FUNCAO, JUNTAMENTE COM A kernel NA VARIAVEL im_filter_mask
    dilate = cv2.dilate(erode, kernel, iterations=1)                       # CRIANDO A VARIAVEL dilate, PEGANDO A FUNCAO dilate DA BIBLIOTECA cv2, E APLICANDO-A A VARIAVEL erode, IMPLEMENTANDO TAMBEM A kernel
    gaussian = cv2.GaussianBlur(dilate, (5, 5), 1)                             # CRIANDO A VARIAVEL gaussian, PEGANDO A FUNCAO GaussianBlur DA BIBLIOTECA cv2, APLICANDO-A A VARIAVEL dilate E COLOCANDO A INTENSIDADE DO FILTRO
    final = cv2.erode(gaussian, np.ones((3, 3), np.uint8), iterations=1)    # CRIANDO A VARIAVEL final, PEGANDO A FUNCAO erode DA BIBLIOTECA cv2, E APLICANDO-A A VARIAVEL gaussian, E EXECUTANDO A MESMA FUNCAO DA kernel, MAS NO PROPRIO COMANDO, POIS NAO SERA NECESSARIO USAR ISSO NOVAMENTE

    return final                                                            # RETORNA A VARIAVEL final NO FINAL DA EXECUCAO DA FUNCAO filter_mask


def update(*arg):                                                                                        # CRIA UMA FUNCAO CHAMADA update
    h0 = cv2.getTrackbarPos('h min', 'control')                                                          # CRIA UMA VARIAVEL h0 COM A FUNCAO getTrackbarPos DA BIBLIOTECA cv2 COM O NOME DO CONTROLE E O NOME DA JANELA
    h1 = cv2.getTrackbarPos('h max', 'control')                                                          # CRIA UMA VARIAVEL h1 COM A FUNCAO getTrackbarPos DA BIBLIOTECA cv2 COM O NOME DO CONTROLE E O NOME DA JANELA
    s0 = cv2.getTrackbarPos('s min', 'control')                                                          # CRIA UMA VARIAVEL s0 COM A FUNCAO getTrackbarPos DA BIBLIOTECA cv2 COM O NOME DO CONTROLE E O NOME DA JANELA
    s1 = cv2.getTrackbarPos('s max', 'control')                                                          # CRIA UMA VARIAVEL s1 COM A FUNCAO getTrackbarPos DA BIBLIOTECA cv2 COM O NOME DO CONTROLE E O NOME DA JANELA
    v0 = cv2.getTrackbarPos('v min', 'control')                                                          # CRIA UMA VARIAVEL v0 COM A FUNCAO getTrackbarPos DA BIBLIOTECA cv2 COM O NOME DO CONTROLE E O NOME DA JANELA
    v1 = cv2.getTrackbarPos('v max', 'control')                                                          # CRIA UMA VARIAVEL v1 COM A FUNCAO getTrackbarPos DA BIBLIOTECA cv2 COM O NOME DO CONTROLE E O NOME DA JANELA

    lower = np.array((h0, s0, v0))                                                                         # CRIA UMA VARIAVEL lower COM A FUNCAO array DA BIBLIOTECA np QUE DEFINE OS VALORES MINIMOS COMO AS VARIAVEIS h0, s0, v0
    upper = np.array((h1, s1, v1))                                                                         # CRIA UMA VARIAVEL upper COM A FUNCAO array DA BIBLIOTECA np QUE DEFINE OS VALORES MAXIMOS COMO AS VARIAVEIS h1, s1, v1

    mask = cv2.inRange(hsv, lower, upper)                                                                # CRIA UMA VARIAVEL mask COM A FUNCAO inRange DA BIBLIOTECA cv2 QUE RECEBE OS VALORES DO FILTRO hsv QUE ESTA NA FUNCAO main, E AS VARIAVEIS lower E upper

    mask_b = cv2.bitwise_not(mask)                                                                      # CRIA UMA VARIAVEL mask_b COM A FUNCAO bitwise_not DA BIBLIOTECA cv2, E APLICA-A A VARIAVEL mask. A FUNCAO DESSA VARIAVEL mask_b E INVERTER AS CORES DA mask
    mask_b = filter_mask(mask_b)                                                                         # APLICA NA mask_b OS VALORES DA FUNCAO filter_mask APLICADOS NA VARIAVEL mask_b

    result_filter = filter_mask(mask)                                                                     # CRIA UMA VARIAVEL result_filter QUE RECEBE A mask COM OS FILTROS DA FUNCAO filter_mask
    result_mask_b = cv2.bitwise_and(result_filter, mask_b)                                                # CRIA UMA VARIAVEL result_mask_b COM A FUNCAO bitwise_and DA BIBLIOTECA cv2 QUE INVERTE OS VALORES BINARIOS DA VARIAVEIS result_filter E mask_b

    im2, contours, hierarchy = cv2.findContours(result_filter, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)     # ENCONTRA OS CONTORNOS DA VARIAVEL result_filter
    total_contours = len(contours)                                                                       # CONTA A QUANTIDADE DE CONTORNOS ENCONTRADOS NA VARIAVEL contours
    result = src.copy()                                                                                  # CRIA UMA VARIAVEL result QUE RECEBE OS CONTORNOS
    cv2.drawContours(result, contours, 1, (0, 255, 0), 3)                                                  # USA A FUNCAO drawContours DA BIBLIOTECA cv2, APLICANDO OS contours NA IMAGEM result, ESCOLHENDO O TIPO 1 DE CONTORNO, ESCOLHENDO A COR VERDE (0, 255, 0) NA ESPESSURA 3

    contours_error = []                                                                                  # CRIA A FUNCAO contours_error E DA UM VALOR VAZIO A ELA

    area_total = 0                                                                                       # CRIA A VARIAVEL area_total E DA O VALOR ZERO A ELA

    contours_ok = []                                                                                       # CRIA A FUNCAO contours_ok E DA UM VALOR VAZIO A ELA
    for cont_contours in contours:                                                                       # CRIA UM for PARA A VARIAVEL cont_contours DENTRO DA VARIAVEL contours
        area = cv2.contourArea(cont_contours)                                                           # CRIA UMA VARIAVEL area QUE RECEBE OS CONTORNOS contourArea DA BIBLIOTECA cv2 E APLICA NA VARIAVEL cont_contours
        print ("area: ", area)                                                                           # MOSTRA NO TERMINAL A "area" DE CADA area CONTORNADA

        area_total += area                                                                               # A VARIAVEL area_total RECEBE ELA MESMO MAIS A area

        if (area > 1000 and area < 1500):                                                                # SE A VARIAVEL area E MAIOR QUE 30 E MENOR QUE 700
            contours_ok.append(cont_contours)                                                            # A VARIAVEL contours_ok E ADICIONADA NA cont_contours
        else:                                                                                            # SENAO
            print ("ERROR_AREA: ", area)                                                                 # MOSTRA NO TERMINAL A MENSAGEM "ERROR_AREA: ", ALEM DE MOSTRAR O TAMANHO DA area QUE ESTA FORA DA CONDICAO
            contours_error.append(cont_contours)                                                         # A VARIAVEL contours_error E ADICIONADA NA cont_contours

    media = area_total / 1	                                                                             # CRIA A VARIAVEL media QUE RECEBE A area_total DIVIDIA PELA QUANTIDADE DE contours

    cv2.drawContours(result, contours_ok, -1, (0, 255, 0), 2)                                              # USA A FUNCAO drawContours DA BIBLIOTECA cv2 PARA APLICAR O DESENHO DO CONTORNO NA VARIAVEL contours_ok COM A COR VERDE (0, 255, 0) NA ESPESSURA 2
    cv2.drawContours(result, contours_error, -1, (0, 0, 255), 2)                                           # USA A FUNCAO drawContours DA BIBLIOTECA cv2 PARA APLICAR O DESENHO DO CONTORNO NA VARIAVEL contours_error COM A COR VERMELHA (0, 0, 255) NA ESPESSURA 2

    print ("\tTOTAL DE ARVORES: ", total_contours)                                                        # MOSTRA NO TERMINAL O "TOTAL DE ARVORES", ALEM DA VARIAVEL total_contours (O \t FUNCIONA PARA DAR UM "TAB" NO TERMINAL)
    print ("\tARVORES: ", len(contours_ok))                                                              # MOSTRA NO TERMINAL AS "ARVORES", ALEM DA VARIAVEL contours_ok COM A FUNCAO len, QUE SERVE PARA CONTAR A QUANTIDADE DE CONTORNOS (O \t FUNCIONA PARA DAR UM "TAB" NO TERMINAL)
    print ("\tERROS: ", len(contours_error))                                                             # MOSTRA NO TERMINAL OS "ERROS", ALEM DA VARIAVEL contours_error COM A FUNCAO len, QUE SERVE PARA CONTAR A QUANTIDADE DE CONTORNOS (O \t FUNCIONA PARA DAR UM "TAB" NO TERMINAL)
    print ("\tAREA MEDIA: ", media)                                                                      # MOSTRA NO TERMINAL A "AREA MEDIA", ALEM DA VARIAVEL media

    cv2.imshow('mask', mask)                                                                             # USA A FUNCAO imshow DA BIBLIOTECA cv2 PARA MOSTRAR NA TELA A VARIAVEL mask

    cv2.imshow('RESULT_FILTER: ', result_filter)                                                         # USA A FUNCAO imshow DA BIBLIOTECA cv2 PARA MOSTRAR NA TELA A VARIAVEL result_filter

    cv2.imshow('result_mask_b:', result_mask_b)                                                         # USA A FUNCAO imshow DA BIBLIOTECA cv2 PARA MOSTRAR NA TELA A VARIAVEL result_mask_b

    cv2.imshow('result:', result)                                                                       # USA A FUNCAO imshow DA BIBLIOTECA cv2 PARA MOSTRAR NA TELA A VARIAVEL result


def main():                                                                                              # CRIA A FUNCAO PRINCIPAL, CHAMADA main
    cv2.namedWindow('control', 0)                                                                        # CRIA UMA JANELA CHAMADA control
    cv2.createTrackbar('h min', 'control', 21, 255, update)                                              # ADICIONA A TELA control UM TRACKBAR CHAMADA h min COM O VALOR INICIAL IGUAL A 21 E O FINAL 255, PEGANDO AS INFORMACOES DA FUNCAO update
    cv2.createTrackbar('h max', 'control', 55, 255, update)                                              # ADICIONA A TELA control UM TRACKBAR CHAMADA h max COM O VALOR INICIAL IGUAL A 55 E O FINAL 255, PEGANDO AS INFORMACOES DA FUNCAO update
    cv2.createTrackbar('s min', 'control', 30, 255, update)                                              # ADICIONA A TELA control UM TRACKBAR CHAMADA s min COM O VALOR INICIAL IGUAL A 30 E O FINAL 255, PEGANDO AS INFORMACOES DA FUNCAO update
    cv2.createTrackbar('s max', 'control', 138, 255, update)                                             # ADICIONA A TELA control UM TRACKBAR CHAMADA s max COM O VALOR INICIAL IGUAL A 138 E O FINAL 255, PEGANDO AS INFORMACOES DA FUNCAO update
    cv2.createTrackbar('v min', 'control', 100, 255, update)                                             # ADICIONA A TELA control UM TRACKBAR CHAMADA v min COM O VALOR INICIAL IGUAL A 100 E O FINAL 255, PEGANDO AS INFORMACOES DA FUNCAO update
    cv2.createTrackbar('v max', 'control', 192, 255, update)                                             # ADICIONA A TELA control UM TRACKBAR CHAMADA v max COM O VALOR INICIAL IGUAL A 192 E O FINAL 255, PEGANDO AS INFORMACOES DA FUNCAO update

    im = cv2.resize(src, None, fx=0.25, fy=0.25, interpolation=cv2.INTER_CUBIC)                          # UTILIZA A FUNCAO resize DA BIBLIOTECA cv2 PARA REDIMENSIONAR A VARIAVEL im EM 25% (0.25)
    cv2.imshow('control', im)                                                                             # USA A FUNCAO imshow DA BIBLIOTECA cv2 PARA ADICIONAR A VARIAVEL im NA JANELA CHAMADA CONTROL
    update()                                                                                             # CHAMA A FUNCAO update
    while 1:                                                                                             # CRIA UM LOOP INFINITO
        ch = cv2.waitKey(30)                                                                             # CRIA UMA VARIAVEL ch COM A FUNCAO waitKey DA BIBLIOTECA cv2 PARA ESPERAR A TECLA 30 DO TECLADO (esc)
        if (ch == 27):                                                                                   # CRIA UMA CONDICAO PARA QUE SE A VARIAVEL ch RECEBER A TECLA 27 (esc) EXECUTAR A SEGUINTE ATIVIDADE
            break                                                                                        # PARA A APLICACAO
            cv2.destroyAllWindows()                                                                      # UTILIZA A FUNCAO destroyAllWindows DA BIBLITOECA cv2 PARA DESTRUIR TODAS AS JANELAS ABERTAS PELA APLICACAO


if __name__ == '__main__':                                                                               # CRIA UMA CONDICAO
    import sys                                                                                           # IMPORTA A BIBLIOTECA DO SISTEMA (sys) DENTRO DA PROPRIA CONDICAO
    try:                                                                                                 # EXECUTA AS PROXIMAS DUAS LINHAS DE COMANDO SE FOREM POSITIVAS
        fn = sys.argv[1]                                                                                 # CRIA A VARIAVEL fn PRA RECEBER O ARGUMENTO argv COM A BIBLIOTECA sys
        print("parametro:", fn)                                                                          # CASO O USUARIO DIGITE UM PARAMETRO NA FRENTE DO NOME DA APLICACAO NO TERMINAL, COMO UMA IMAGEM, POR EXEMPLO, ELA SERA SALVA NA VARIAVEL fn
    except Exception as e:                                                                               # CASO O try NAO SEJA FEITO EXECUTA A PROXIMA LINHA
        fn = 'DJI_0016.JPG'                                                                              # CRIA A VARIAVEL fn E DA A ELA O VALOR trosp_1.png, QUE NO CASO, E UMA IMAGEM DENTRO DA PASTA QUE O ARQUIVO (trosp_1.py) ESTA SALVO

    src = cv2.imread(fn)	                                                                             # CRIA UMA VARIAVEL src QUE RECEBE A FUNCAO imread DA BIBLIOTECA cv2, RECEBENDO O VALOR DA VARIVEL fn
    src = cv2.resize(src, None, fx=0.25, fy=0.25, interpolation=cv2.INTER_CUBIC)                         # UTILIZA A FUNCA resize DA BIBLIOTECA cv2 PARA REDIMENSIONAR A VARIAVEL src EM 25% (0.25)
    print("resized shape:", src.shape)                                                                   # MOSTRA NA TELA A VARIAVEL src COM O TAMANHO JA ATUALIZADO
    hsv = cv2.cvtColor(src, cv2.COLOR_BGR2HSV)                                                           # CRIA UMA VARIAVEL hsv QUE RECEBE A FUNCAO cvtColor DA BIBLIOTECA cv2, FUNCAO ESSA QUE CONVERTE A VARIAVEL src DE RBG (RED, GREEN, BLUE) PARA HSV (HUE, SATURATION, VALUE)
    main()                                                                                               # CHAMA A FUNCAO main
